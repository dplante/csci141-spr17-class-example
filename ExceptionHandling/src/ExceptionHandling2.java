
public class ExceptionHandling2
{

    public static void main(String[] args)
    {
        System.out.println("Hi, I'm here!");
        
        /*
         * Thread is a compile time exception, so you
         * MUST use a try/catch to even get your code
         * to compile.
         */
        try
        {
            Thread.sleep(20);
        } catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println("Now I'm there!");

        float numerator, denominator;
        numerator = 14.0f;
        denominator = 0.0f;
        
        /*
         * Arithmetic exception is a "runtime" exception;
         * you can code it without a try/catch, but it will
         * fail if you divide by zero.
         */
        int result = 0;
        try
        {
            result = (int)numerator/(int)denominator;
        }
        catch (ArithmeticException e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println(numerator + " / " + denominator + " = " + result);
    }

}

/**
 * One example of using the Scanner class to input strings and 
 * print them out.
 * 
 * @author dplante
 *
 */
public class ExceptionHandling1 
{
    public static void main(String[] args)
    {   
        java.util.Scanner sc = new java.util.Scanner(System.in);
        
        System.out.println("Enter a number: ");
        
        int value = 0;
        
        try
        {
            value = sc.nextInt();
        }
        catch (java.util.InputMismatchException ex)
        {
            ex.printStackTrace();
        }
        
        System.out.println("Value = " + value);
        
        sc.close();
    }

}

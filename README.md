# README #

This repository is the collection of sample projects and programs that we developed or went 
over in class during the Spring 2017 semester for the CSCI141 class.

### What is this repository for? ###

* Review if you were in the class with me
* See what we covered when I taught the class
* Get an example of a repository that is public but cannot be changed by you

import javax.swing.JOptionPane;

/**
 * Basic JOptionPane input
 * 
 * @author dplante
 *
 */
public class JOptionPaneInput
{

	public static void main(String[] args) 
	{
		int value = 0;
		boolean valid = false;
		
		// Input using JOptionPane class
		String message = "Enter a number: ";
		
		while (!valid)
		{
			String str = JOptionPane.showInputDialog(message);

			try
			{
				value = Integer.parseInt(str);
				valid = true;
			}
			catch (NumberFormatException e)
			{
				message = "Not a number, try again.";
			}
		}
		
		// To do this, see examples in Java API page for JOptionPane.
		String output = "You entered the number " + value;
		JOptionPane.showMessageDialog(null, output, "Your Stuff!", JOptionPane.INFORMATION_MESSAGE);
		
	}

}

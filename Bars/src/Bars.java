import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * Perrrtty GUI for drawing bars
 * 
 * @author Daniel Plante
 *
 */

@SuppressWarnings("serial")
public class Bars extends JFrame
{
    /*
     * Properties
     */
    private JButton myTopButton, myBottomButton;
    private JPanel myBarPanel;
    private Bar[] myBars;

    /*
     * Methods
     */
    public static void main(String[] args)
    {
        new Bars();
    }

    public Bars()
    {
        /*
         * Set layout
         * 
         * See: https://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html
         * 
         * for different layouts
         */
         Container pane = this.getContentPane();
         pane.setLayout(new BorderLayout());
        
        myTopButton = new JButton("Top");
        myBottomButton = new JButton("Bottom");
        pane.add(myTopButton, BorderLayout.PAGE_START);
        pane.add(myBottomButton, BorderLayout.PAGE_END);

        // Panel
        myBarPanel = new JPanel();
        myBarPanel.setLayout(new GridLayout(1, 3));
        myBarPanel.setPreferredSize(new Dimension(150, 200));

        myBars = new Bar[3];
        for (int i = 0; i < 3; i++)
        {
            Color color = new Color(i * 70, 0, 0);
            myBars[i] = new Bar(50, 200, color);
            myBarPanel.add(myBars[i]);
        }

        pane.add(myBarPanel, BorderLayout.CENTER);
        this.pack();

        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
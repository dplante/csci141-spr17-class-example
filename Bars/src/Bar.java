import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Colored bar patterned completely off off CirclesCanvas
 * 
 * @author Daniel Plante
 * 
 * Date: 9 September 2011
 * Updated: 23 March 2017
 *
 */

public class Bar extends Canvas
{
   /*
    * Properties
    */

   private Color myColor;
   
   private int myWidth;
   private int myHeight;

   /*
    * Methods
    */
   /**
    * 
    */
   public Bar(int xSize, int ySize, Color color)
   {
      myColor = color;
      myWidth = xSize;
      myHeight = ySize;
   }

   /**
    * Method to draw the bar by passing it back through the 
    * Graphics object passed in.
    * 
    * @param g Graphics object
    */
   public void paint(Graphics g)
   {
      g.setColor(myColor);
      g.fillRect(0, 0, myWidth, myHeight);
   }

}

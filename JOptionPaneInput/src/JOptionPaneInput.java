import javax.swing.JOptionPane;

/**
 * Basic JOptionPane input
 * 
 * @author dplante
 *
 */
public class JOptionPaneInput
{

	public static void main(String[] args) 
	{
		int value;
		
		// Input using JOptionPane class
		String message = "Enter a number: ";
		String str = JOptionPane.showInputDialog(message);
		System.out.println(str);

		value = Integer.parseInt(str);
		
		float valueFloat = Float.valueOf(str);
		
		System.out.println("float = " + valueFloat);
		value++;
		System.out.println("Value = " + value);
	}

}

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * A Smile application that draws a smiling face in
 * the window.
 * 
 * @author dplante
 *
 */
public class SmileHarder extends JFrame
{
	/*
	 * Your main class that is required in every
	 * application you write.
	 */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() 
        {
            public void run() 
            {
                smileGui();
            }
        });
    }

    public static void smileGui() 
    {
        JFrame smile = new JFrame("Smiley Face Harder");
        DrawPanel panel = new DrawPanel();
        smile.add(panel);
        smile.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        smile.setBackground(Color.CYAN);
        smile.setSize(500,500);
        smile.pack();
        smile.setVisible(true);
    }
}

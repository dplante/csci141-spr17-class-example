import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
     * Part of the Abstract Widget Toolkit, the paint()
     * "method" is called to display the graphics.
     */
    public class DrawPanel extends JPanel
    {
        public Dimension getPreferredSize()
        {
            return new Dimension(500, 500);
        }
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            g.setColor(Color.RED);
            g.drawOval(100, 50, 200, 200);
            g.fillOval(155, 100, 10, 20);
            g.fillOval(230, 100, 10, 20);
            g.drawArc(150, 160, 100, 50, 180, 180);
        }
    }

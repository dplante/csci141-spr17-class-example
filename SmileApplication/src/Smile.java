import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 * A Smile application that draws a smiling face in
 * the window.
 * 
 * @author dplante
 *
 */
public class Smile extends JFrame
{
	/*
	 * Your main class that is required in every
	 * application you write.
	 */
	public static void main(String[] args) 
	{
		Smile smile = new Smile();
	}
	
	/**
	 * The Smile "constructor" that creates an "instance" of 
	 * the "class" (i.e. an "object")
	 */
	public Smile()
	{
	    /*
	     * See if students can have this work; other classes had trouble
	     * with view not showing up properly.  If this does not work,
	     * try SmileNew.java which worked well in Spring 2017.  If that
	     * does not work, go for SmileHarder.java and DrawPanel.java, 
	     * which should work but is less clean for early in the course.
	     */
		this.setBackground(Color.CYAN);
		this.setSize(500, 500);
        this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Part of the Abstract Widget Toolkit, the paint()
	 * "method" is called to display the graphics.
	 */
	public void paint(Graphics g)
	{
		g.setColor(Color.RED);
		g.drawOval(100, 50, 200, 200);
		g.fillOval(155, 100, 10, 20);
		g.fillOval(230, 100, 10, 20);
		g.drawArc(150, 160, 100, 50, 180, 180);
	}

}

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * A Smile application that draws a smiling face in
 * the window.
 * 
 * @author dplante
 *
 */
public class SmileNew extends JFrame
{
	/*
	 * Your main class that is required in every
	 * application you write.
	 */
	public static void main(String[] args) 
	{
		new SmileNew();
	}
	
	/**
	 * The Smile "constructor" that creates an "instance" of 
	 * the "class" (i.e. an "object")
	 */
	public SmileNew()
	{
	    super("Smiley Face");
	    
	    this.setContentPane(new DrawPanel());
		this.setSize(500, 500);
        this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Part of the Abstract Widget Toolkit, the paint()
	 * "method" is called to display the graphics.
	 */
	public class DrawPanel extends JPanel
	{
    	public void paintComponent(Graphics g)
    	{
            g.setColor(Color.RED);
            g.drawOval(100, 50, 200, 200);
            g.setColor(Color.BLUE);
            g.fillOval(155, 100, 10, 20);
            g.fillOval(230, 100, 10, 20);
            g.drawArc(150, 160, 100, 50, 180, 180);
    	}
	}

}

import java.text.DecimalFormat;

public class Hello 
{
	public static void main(String[] args) 
	{
		System.out.println("Hello");
		
		double pi = 3.1415926;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		System.out.println(df.format(pi));
	}

}

/*
 * Show that this class cannot be run using "Run as" because no main().
 */
public class Dog 
{
	public void bark()
	{
		System.out.println("Woof!");
	}
}

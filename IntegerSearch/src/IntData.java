/**
 * Integer data searching class
 * 
 * @author dplante
 *
 */
public class IntData
{
    public final static int NO_DATA = -1;
    private int[] myData = {7, 3, 29, -5, 48, 36, 7, 2};
    
    public IntData()
    {
        
    }
    
    /**
     * Perform a linear search to find the passed-in value
     * in the array of data stored in this class.
     * 
     * @param value number we are looking for
     * @return the index where the value exists, or NO_DATA if it does not.
     */
    public int search(int value)
    {
        /*
         * Don't use "magic numbers" like -1; instead, use
         * a constant.
         */
        int index = 0;
        int maxLength = myData.length;
        boolean found = false;
        
        /*
         * Linear search for value in array
         */
        while (!found && index < maxLength)
        {
            if (value == myData[index])
            {
                found = true;
            }
            else
            {
                index++;
            }
        }
        
        if (index == maxLength)
        {
            index = NO_DATA;
        }
        
        return index;
    }

}


public class Main
{

    public static void main(String[] args)
    {
        IntData intData = new IntData();
        
        int valueToFind = 7;
        int index = intData.search(valueToFind);
        
        if (index == IntData.NO_DATA)
        {
            System.out.println("Value " + valueToFind + " not found!");
        }
        else
        {
            System.out.println("Value " + valueToFind + " found at index " + index);
        }
    }
}

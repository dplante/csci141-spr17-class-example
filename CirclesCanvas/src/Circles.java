import java.awt.*;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
/**
 * Circles class that draws Circles, but THESE Circles are
 * extending Canvas, instead actually making the Circles
 * canvases
 * 
 * @author dplante
 *
 */
public class Circles 
{
   ///////////////////////////
   //       Properties      //
   ///////////////////////////

   private Circle myCircle1,
                  myCircle2;
   private JPanel myPanel;

   ///////////////////////////
   //         Methods       //
   ///////////////////////////

   public static void main(String[] args) 
   {
       new Circles();
   }
   
   public Circles()
   {  
      Point center1 = new Point(120, 120);
      Point center2 = new Point(100, 100);
      myCircle1 = new Circle(40, center1, Color.black,
                                           Color.yellow, true);
      myCircle2 = new Circle(30, center2, Color.green, 
                                           Color.cyan, true);
      myCircle1.setBackground(Color.red);
      myCircle2.setBackground(Color.pink);
      myCircle1.setSize(100,200);
      myCircle2.setSize(100,200);
      
      myPanel = new JPanel(new GridLayout(1,2));
      
      myPanel.add(myCircle1);
      myPanel.add(myCircle2);
      
      /*
       * Notice that we create a JFrame here!  We did not 
       * extend JFrame like in other examples up until now!
       * So instead of using "this.setVisible(true)", for example,
       * we use "frame.setVisible(true)" instead.  "this" was
       * referring to THIS JFrame object (which we created when
       * we "instantiated" the "Circles extends JFrame" class.
       * 
       * ASK STUDENTS:  Did you use "inheritance" in CSCI111Q?
       * 
       */
      Dimension dimension = new Dimension(500,500);
      JFrame frame = new JFrame();
      frame.setSize(dimension);
      //myPanel.setBounds(10,20,500,400);
      frame.add(myPanel);
      frame.setVisible(true);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

   }

   /**
    * Method to draw the paintable components
    * @author dplante
    *
    * NOTICE: Because Circle extends Canvas, no need to 
    * use paint() or paintComponent() here!!
    */
//   public class DrawPanel extends JPanel
//   {
//       public void paintComponent(Graphics g)
//       {
//           myCircle1.drawIt(g);
//           myCircle2.drawIt(g);
//       }
//   }
}
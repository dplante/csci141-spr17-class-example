import java.awt.*;

/**
 * Circle class that can draw itself given properties of
 * center point, radius, and the color to fill and "not fill"
 * it with.
 * 
 * NOTE: to make this work as a paintable object, must extend Canvas
 * and change drawIt() to paint().
 * 
 * @author Daniel Plante
 * 
 * Date: 9 September 2011
 *
 */

public class Circle extends Canvas
{
   /*
    * Properties
    */

   private int myRadius;

   private Point myCenterPoint;

   private Color myFilledColor,
                 myUnfilledColor;

   private boolean myIsItFilled;

   /*
    * Methods
    */
   /**
    * Default constructor to draw a filled circle
    */
   public Circle()
   {
      myCenterPoint = new Point(100,100);
      myRadius = 50;
      myFilledColor = Color.red;
      myUnfilledColor = Color.blue;
      myIsItFilled = true;
   }

   /**
    * Constructor to draw a circle with various properties of 
    * that circle passed in as arguments.
    * 
    * @param radius radius of the circle
    * @param centerPoint the center point of the circle assuming the coordinate
    *        system begins with (0,0) being upper left corner of view
    * @param filledColor color if the circle will be filled
    * @param unfilledColor color for unfilled circle
    * @param isItFilled boolean determining if the circle is filled or not
    */
   public Circle(int radius, Point centerPoint, 
                 Color filledColor, Color unfilledColor,
                 boolean isItFilled)
   {
      myCenterPoint = centerPoint;
      myRadius = radius;
      myFilledColor = filledColor;
      myUnfilledColor = unfilledColor;
      myIsItFilled = isItFilled;
   }

   /**
    * Method to draw the circle by passing it back through the 
    * Graphics object passed in.
    * 
    * @param g Graphics object
    */
   public void paint(Graphics g)
   {
      int width, height;
      int x, y, xCenter, yCenter;

      width = 2*myRadius;
      height = width;

      xCenter = myCenterPoint.x;
      yCenter = myCenterPoint.y;

      x = xCenter - myRadius;
      y = yCenter - myRadius;

      if(myIsItFilled)     // same as if(myIsItFilled == true)
      {
         g.setColor(myFilledColor);
      }
      else
      {
         g.setColor(myUnfilledColor);
      }

      g.fillOval(x, y, width, height);
   }

   /*
    * Accessors and mutators
    */
   public void setFilledColor(Color color)
   {
      myFilledColor = color;
   }

   public Color getFilledColor()
   {
      return myFilledColor;
   }

   public void setUnfilledColor(Color color)
   {
      myUnfilledColor = color;
   }

   public Color getUnfilledColor()
   {
      return myUnfilledColor;
   }

   public void setRadius(int radius)
   {
      myRadius = radius;
   }

   public int getRadius()
   {
      return myRadius;
   }

   public void setCenterPoint(Point point)
   {
      myCenterPoint = point;
   }

   public Point getCenterPoint()
   {
      return myCenterPoint;
   }

   public void setIsItFilled(boolean isItFilled)
   {
      myIsItFilled = isItFilled;
   }

   public boolean getIsItFilled()
   {
      return myIsItFilled;
   }
}

/**
 * Colors class to illustrate use of ActionListener and some GUI objects
 * 
 * @author Daniel Plante
 */

import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.JButton;

import java.awt.*;

// Don't forget to import the event classes
import java.awt.event.*;

// Don't forget to implement the actionListener.
@SuppressWarnings("serial")
public class Colors extends JFrame implements ActionListener
{
    /*
     * Properties
     */
    private static final int WIDTH = 500, HEIGHT = 300;
    private String myText;
    private Font myTextFont;
    private int myPointSize, myApproxTextWidth, myApproxTextHeight;
    private int myColorOfText, myColorOfBackground;
    private JButton myTextButton, myBackgroundButton;

    /////////////////////////////////////
    // class methods //
    /////////////////////////////////////

    public static void main(String[] args)
    {
        Colors colors = new Colors();
        colors.setVisible(true);
    }

    public Colors()
    {
        myColorOfText = 0;
        myColorOfBackground = 0;

        // Set size of application
        this.setSize(WIDTH, HEIGHT);

        // Set text, its size, and its font
        myText = "Goodbye World! Hello Java!!";
        myPointSize = 28;

        myApproxTextWidth = (myText.length() * myPointSize) / 4;
        myApproxTextHeight = myPointSize;
        myTextFont = new Font("Helvetica", Font.BOLD, myPointSize);

        // Instantiates my buttons.
        myTextButton = new JButton("Text Color");
        myTextButton.setSize(WIDTH / 4, HEIGHT / 4);
        myBackgroundButton = new JButton("Background Color");
        myBackgroundButton.setSize(WIDTH / 4, HEIGHT / 4);

        // Adds actionListener to the button and listens in
        // "this" object
        myTextButton.addActionListener(this);
        myBackgroundButton.addActionListener(this);

        this.setContentPane(new DrawPanel());
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new FlowLayout());

        // Adds buttons to the view
        contentPane.add(myTextButton);
        contentPane.add(myBackgroundButton);
        
        this.changeBackgroundColor(myColorOfBackground);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    // Once an action is implemented, here's what to do....
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == myBackgroundButton)
        {
            myColorOfBackground = ++myColorOfBackground % 5;
        } else if (e.getSource() == myTextButton)
        {
            myColorOfText = ++myColorOfText % 3;
        }

        this.changeBackgroundColor(myColorOfBackground);
        this.repaint();
    }

    /*
     * Change the background color within the the JFrame itself, unlike
     * changing the color of the text drawn in the Graphics context, which is
     * done in a JPanel using the paintComponent() method.
     */
    public void changeBackgroundColor(int background)
    {
        // Determine what color to make the background
        switch (background) {
        case 0:
            this.setBackground(Color.cyan);
            break;
        case 1:
            this.setBackground(Color.orange);
            break;
        case 2:
            this.setBackground(Color.red);
            break;
        case 3:
            this.setBackground(Color.black);
            break;
        case 4:
            this.setBackground(Color.green);
            break;
        default:
            this.setBackground(Color.white);
            break;
        }
    }

    /*
     * To update the graphics context, do so through a JPanel
     */
    public class DrawPanel extends JPanel
    {
        public void paintComponent(Graphics g)
        {
            Dimension applicationDimension;
            int stringXPoint, stringYPoint;

            // Determine where to draw text on screen

            applicationDimension = this.getSize();
            stringXPoint = (applicationDimension.width / 2) - myApproxTextWidth;
            stringYPoint = (applicationDimension.height / 2) + myApproxTextHeight;

            // Determine the colors
            this.determineTextColor(g);

            // Draw the text at the appropriate point
            g.setFont(myTextFont);
            g.drawString(myText, stringXPoint, stringYPoint);
        }

        // Determine the text color for the given button click.
        public void determineTextColor(Graphics g)
        {
            // Determine what color to make the text
            switch (myColorOfText) {
            case 0:
                g.setColor(Color.blue);
                break;
            case 1:
                g.setColor(Color.magenta);
                break;
            case 2:
                g.setColor(Color.green);
                break;
            default:
                g.setColor(Color.blue);
                break;
            }
        }
    }
}

import javax.swing.JOptionPane;

/**
 * How to throw exceptions
 * 
 * @author dplante
 *
 */
public class ExceptionThrower
{

	public static void main(String[] args) 
	{
		int value = 0;
		boolean cont = true;
		
		while (cont)
		{
    		try
    		{
    			value = IntegerInput.input(10, 20);
    			// To do this, see examples in Java API page for JOptionPane.
    			String output = "You entered the number " + value;
    			JOptionPane.showMessageDialog(null, output, "Your Stuff!", JOptionPane.INFORMATION_MESSAGE);
    			cont = !cont;
    		}
    		catch (OutOfRangeException e)
    		{
    			System.out.println(e.getMessage());
    		}
		}
	}
}

import javax.swing.JOptionPane;

public class IntegerInput 
{
	public static int input(int min, int max) throws OutOfRangeException
	{
		OutOfRangeException exception = new OutOfRangeException("Value is out of range");
		
		int value = 0;
		boolean valid = false;
		
		// Input using JOptionPane class
		String message = "Enter a number: ";
		
		while (!valid)
		{
			String str = JOptionPane.showInputDialog(message);

			try
			{
				value = Integer.parseInt(str);
				valid = true;
			}
			catch (NumberFormatException e)
			{
				message = "Not a number, try again.";
			}
		}
		
		if (value < min || value > max)
		{
			throw exception;
		}
		return value;
	}

}

import java.util.Scanner;

/**
 * One example of using the Scanner class to input strings and 
 * print them out.
 * 
 * @author dplante
 *
 */
public class ScannerInput1 
{
	public static void main(String[] args)
	{
	    /*
	     * Below, understand the difference between "declare", "initialize"
	     * and "instantiate".
	     */
	    
	    // just declare a variable "str"
        String str = "";  
        
        // declare and initialize an integer (though we won't use it below)
        int value = 4;
        
	    /*
	     * Below, could replace
	     *  
         * Scanner sc = new Scanner(System.in);
         * 
         * with
	     * 
	     * Scanner sc = new Scanner("Daniel Plante");
	     * 
	     * but with your own first and last name.  Scanner
	     * takes a "stream", so "System.in" works, "Daniel Plante"
	     * works, as does a file stream, which we'll cover later
	     * in the course.
	     */
	    /*
	     * Declare and "instantiate" an object
	     */
		Scanner sc = new Scanner(System.in);
		
        System.out.println("Enter your first and last name: ");

		/*
		 * Instead of a space between words entered, using a different pattern
		 * (e.g. a # sign like below), you can enter "Daniel#Plante#" to repeat
		 * the same process.
		 */
		//sc.useDelimiter("#");
		
		str = sc.next();
		System.out.println("First Name: " + str);
		
		str = sc.next();
		System.out.println("Last Name : " + str);
		
		/*
		 * Notice that because we specifically ask for two words to be scanned
		 * in (separated by a space), the sc.close() below does close the 
		 * Scanner for taking in input (i.e. the red box showing the code is
		 * running goes away!)
		 */
		sc.close();
	}

}

import java.util.Scanner;

public class Scanner3 
{
	public static void main(String[] args) 
	{	
		System.out.println("Enter words:");
		
		Scanner sc = new Scanner(System.in);
		
		while (sc.hasNextLine())
		{
			String line = sc.nextLine();
			System.out.println(line);
		}
		
		sc.close();
	}

}


public class CompoundInterest 
{
	public static void main(String[] args) 
	{
		/*
		 * Compound interest is determined as follows:
		 * 
		 * A = P(1 + i)^n
		 * 
		 * A - Amount after compounding
		 * P - Principal amount
		 * i - interest rate per period (i.e. i = r/m where r is the
		 *     annual interest rate and m is the number of times
		 *     compounded annually.)
		 * n - number of payments total (i.e. n = m * t where
		 *     as for i above, m is the number of times compounded
		 *     annually and t is the number of years for compounding.)
		 */
		
		float principal, annualPercentRate, numYears, numTimesCompoundedAnnually;
		
		principal = 20000;
		annualPercentRate = 6.5f;
		numTimesCompoundedAnnually = 4;
		numYears = 5;
		
		float interestRateDecimal, numPayments;
		interestRateDecimal = annualPercentRate/(numTimesCompoundedAnnually * 100.0f);
		numPayments = numYears * numTimesCompoundedAnnually;
		
		double amount;
		amount = principal * (float)Math.pow(1.0f + interestRateDecimal, numPayments);
		
		System.out.print("Principal = ");
		System.out.printf(" the amount %15.2f", principal);
		System.out.print(" in dollars\n");
		
		System.out.print("Amount = ");
		System.out.printf("%15.2f", amount);
		System.out.print(" in dollars\n");

	}

}

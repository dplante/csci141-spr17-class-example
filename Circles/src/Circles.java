import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * 
 * Circles class that draws Circles, but THESE Circles are
 * extending Canvas, instead actually making the Circles
 * canvases
 * 
 * Application demonstrate calling a class that can draw itself,
 * as well as using other classes such as Point and Dimension.
 * 
 * @author Daniel Plante
 *
 */
@SuppressWarnings("serial")
public class Circles extends JFrame
{

   /*
    * Properties
    */
   private Circle myCircle1,
                  myCircle2;

   /*
    * Methods
    */
   public static void main(String[] args) 
   {
       new Circles();
   }
   
   public Circles()
   {
	  /*
	   * Point and Dimension are classes you can use. 
	   * Like MANY others, they are written for you.
	   */
      Point center;
      Dimension dimension;
      
      center = new Point(200, 300);
      
      myCircle1 = new Circle();
      myCircle2 = new Circle(75, center, Color.green, Color.cyan, true);

      dimension = new Dimension(500, 500);
      
      this.setContentPane(new DrawPanel());
      this.setSize(dimension);
      this.setVisible(true);
      this.setDefaultCloseOperation(EXIT_ON_CLOSE);

   }

   /**
    * Method to draw the paintable components
    * @author dplante
    *
    */
   public class DrawPanel extends JPanel
   {
       public void paintComponent(Graphics g)
       {
           myCircle1.drawIt(g);
           myCircle2.drawIt(g);
       }
   }
}
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * Example code using lots of GUI components
 * 
 * @author Daniel Plante
 *
 */
@SuppressWarnings("serial")
public class GeneticsInClass extends JFrame
{
    private static final int WINDOW_WIDTH=900;
    private static final int WINDOW_HEIGHT=500;
    private static final int HUGE_FONT_SIZE=36,
                             BIG_FONT_SIZE=24,
                             MEDIUM_FONT_SIZE=14;

    private JButton   myOrderButton;
    private JCheckBox myNervousCheckbox;
    private Color     myColor;
    private Font      myHugeFont,
                      myBigFont,
                      myMediumFont;
    private JLabel    myBehaviorLabel,
                      myAffectionLabel,
                      myHormonesLabel,
                      myGrowMethodsLabel,
                      myMoodsLabel;
    private JComboBox myBehaviorCombo,
                      myAffectionCombo;
    private JList     myHormonesList,
                      myGrowMethodsList;
    private JLabel    myExtraLabel1,
                      myExtraLabel2;

    public static void main(String[] args) 
    {
        new GeneticsInClass();
    }
    
    public GeneticsInClass()
    {
        /*
         * Set up the widgets
         */
        myNervousCheckbox  = new JCheckBox("Have you had a nervous breakdown lately?");
        myOrderButton      = new JButton("Order the Works!");

        myBehaviorLabel    = new JLabel("Behaviors");
        myGrowMethodsLabel = new JLabel("Fun Methods to Grow By");
        myAffectionLabel   = new JLabel("Signs of Affection");
        myHormonesLabel    = new JLabel("Hopin' Hormones");
        myMoodsLabel       = new JLabel("How do you feel right now?");

        /*
         * Set up data for lists and combo boxes
         */
        String[] behaviorData = {"Aggression", "Altruism", "Skepticism", 
                                   "Optimism", "Imitation"};

        String[] growMethodsData = {"Cross over breeding", "Fertilization",
                                      "Hybridization", "Mutation", "Mitosis", "Meiosis"};

        String[] affectionData = {"Kisses", "Hugs", "Caresses", "Punches", "Kicks"};

        String[] hormonesData = {"Testosterone", "Estrogen", "Progesterine"};

        myBehaviorCombo    = new JComboBox(behaviorData);
        myAffectionCombo   = new JComboBox(affectionData);
        myGrowMethodsList  = new JList(growMethodsData);
        myHormonesList     = new JList(hormonesData);

        myHugeFont   = new Font("TimesRoman",Font.BOLD,HUGE_FONT_SIZE);
        myBigFont    = new Font("TimesRoman",Font.BOLD,BIG_FONT_SIZE);
        myMediumFont = new Font("Helvetica",Font.ITALIC,MEDIUM_FONT_SIZE);

        myColor = new Color(0.5f,0.5f,1.0f);
        
        myExtraLabel1 = new JLabel("Tired of living the SLOW life?");
        myExtraLabel2 = new JLabel("Order NOW, you Frankenstein wannabees!");
        myExtraLabel1.setForeground(Color.RED);        
        myExtraLabel2.setForeground(Color.BLUE);        
        
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLabelFonts();
        this.setBackgroundColors();
        
        /*
         * Create panel, give it a size and color, then "add" the components
         * just created into the panel.  Note that this is using FlowLayout, 
         * but clearly it would look better with either BoarderLayout or 
         * GridLayout and multiple Panels.
         */
        JPanel panel = new JPanel();
        panel.add(myBehaviorLabel);
        panel.add(myBehaviorCombo);
        panel.add(myHormonesLabel);
        panel.add(myHormonesList);
        panel.add(myGrowMethodsLabel);
        panel.add(myGrowMethodsList);
        panel.add(myAffectionLabel);
        panel.add(myAffectionCombo);
        panel.add(myNervousCheckbox);
        panel.add(myOrderButton);
        panel.add(myExtraLabel1);
        panel.add(myExtraLabel2);

        panel.setBackground(myColor);
        panel.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        this.getContentPane().add(panel);
        this.pack();
    }
    
    /**
     * Set up fonts
     */
    public void setLabelFonts()
    {
        myBehaviorLabel.setFont(myMediumFont);
        myGrowMethodsLabel.setFont(myMediumFont);
        myAffectionLabel.setFont(myMediumFont);
        myHormonesLabel.setFont(myMediumFont);
        myMoodsLabel.setFont(myMediumFont);
        myExtraLabel1.setFont(myBigFont);
        myExtraLabel2.setFont(myHugeFont);
    }
    
    /**
     * Set up background colors (note how some do not work!)
     */
    public void setBackgroundColors()
    {
        this.setBackground(myColor);

        myBehaviorLabel.setBackground(myColor);
        myGrowMethodsLabel.setBackground(myColor);
        myAffectionLabel.setBackground(myColor);
        myHormonesLabel.setBackground(myColor);
        myMoodsLabel.setBackground(myColor);

        myBehaviorCombo.setBackground(Color.cyan);
        myGrowMethodsList.setBackground(Color.cyan);
        myHormonesList.setBackground(Color.cyan); 
        myAffectionCombo.setBackground(Color.cyan);

        myNervousCheckbox.setBackground(Color.cyan);

        myOrderButton.setBackground(Color.red); 
        myExtraLabel1.setBackground(myColor);
        myExtraLabel2.setBackground(myColor);

    }
    
}

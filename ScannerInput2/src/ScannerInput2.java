import java.util.Scanner;

/**
 * One example of using the Scanner class to input strings and 
 * print them out.
 * 
 * @author dplante
 *
 */
public class ScannerInput2 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter two numbers: ");
		
		int value1, value2, value3;
		
		value1 = sc.nextInt();
		value2 = sc.nextInt();
		
		value3 = value1 + value2;
		System.out.println(value1 + " + " + value2 + " = " + value3);
		
		value3 = value1 / value2;
		System.out.println(value1 + " / " + value2 + " = " + value3);
		
		value3 = value1 % value2;
		System.out.println(value1 + " % " + value2 + " = " + value3);
		
		sc.close();
	}

}

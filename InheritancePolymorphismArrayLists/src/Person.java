
public class Person 
{
    private String myName;
    private int myAge;
    
    public Person(String name, int age)
    {
        myName = name;
        myAge = age;
    }
    
    /**
     * This "toString()" method "overrides" the "toString()"
     * method inherited from the Object class.
     */
    public String toString()
    {
        return this.getClass().getName() + ": name = " + myName + " age = " + myAge;
    }
    
    public boolean equals(Person person)
    {
        if (myName.equals(person.getName()) && myAge == person.getAge())
        {
            return true;
        }
        return false;
    }

    public String getName()
    {
        return myName;
    }

    public void setName(String name)
    {
        this.myName = name;
    }

    public int getAge()
    {
        return myAge;
    }

    public void setAge(int age)
    {
        this.myAge = age;
    }

}


public class MainArrays
{
    public final static int MAX_PERSONS = 5;
    
    public static void main(String[] args)
    {
        /*
         * Comment lines below not really working with "polymorphism"
         * because each variable is being assigned their "concrete"
         * type.
         */
//        Person person = new Person("Bianca", 19);
//        Student student = new Student("Clarissa", 21, "80012345", "Mathematics");
//        Employee employee = new Employee("Tram", 20, "80088888", "Economics", 10000000000.0f);
//        System.out.println(person);
//        System.out.println(student);
//        System.out.println(employee);
        
        /*
         * Because of "polymorphism" all "persons" of all subclass types
         * are ultimately of "Person" type, and through casting, you can
         * get each types "specific" features.  Using "instanceof", you can
         * determine the overall type. 
         */
        Person[] persons;
        persons = new Person[MAX_PERSONS];
        persons[0] = new Employee("Seth", 20, "80088888", "Math", 20000000000.0f);
        persons[1] = new Employee("Bryan", 20, "80088888", "CS", 50000000000.0f);
        persons[2] = new Student("Clarissa", 21, "80012345", "Mathematics");
        persons[3] = new Person("Bianca", 19);
        persons[4] = new Student("NoahGreg", 40, "80012345", "Stuff");
        
        for (int i=0; i<MAX_PERSONS; i++)
        {
            System.out.println(persons[i]);
            if (persons[i] instanceof Student)
            {
                System.out.println("Student " + i 
                        + " has id = " + ((Student)persons[i]).getId());
            }
            if (persons[i] instanceof Employee)
            {
                System.out.println("Employee " + i 
                        + " has salary = " + ((Employee)persons[i]).getSalary());
            }
        }
    }

}

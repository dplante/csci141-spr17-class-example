
public class Student extends Person
{
    private String myId;
    private String myMajor;
    
    public Student(String name, int age, String id, String major)
    {
        super(name, age);
        myId = id;
        myMajor = major;
    }

    /**
     * This "toString()" method "overrides" the "toString()"
     * method inherited from the Object class.
     */
    public String toString()
    {
        return super.toString() + " id = " + myId + " major = " + myMajor;
    }

    public String getId()
    {
        return myId;
    }

    public void setId(String id)
    {
        this.myId = id;
    }

    public String getMajor()
    {
        return myMajor;
    }

    public void setMajor(String major)
    {
        this.myMajor = major;
    }

}

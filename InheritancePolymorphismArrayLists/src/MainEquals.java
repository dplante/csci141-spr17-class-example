
public class MainEquals
{
    public final static int MAX_PERSONS = 5;
    
    public static void main(String[] args)
    {
        Person person = new Person("Bianca", 19);
        Person person2 = new Person("Bianca", 29);
        
        /*
         * Setting person2 to be person actually sets the object to be
         * the EXACT same object in memory.
         */
        //person2 = person;
        
        /*
         * Using == to compare objects does not work unless you really
         * just want to compare if they are the same in memory.  Otherwise,
         * you must use equals().
         * 
         * The same holds with > or <.  You use "compareTo()".
         */
        //if (person == person2)
        
        if (person.equals(person2))
        {
            System.out.println(person.getName() + " is " + person2.getName());
        }
        else
        {
            System.out.println(person.getName() + " is NOT " + person2.getName());
        }
    }

}

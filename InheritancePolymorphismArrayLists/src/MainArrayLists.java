import java.util.ArrayList;

public class MainArrayLists
{ 
    public static void main(String[] args)
    {
        ArrayList<Person> persons;
        persons = new ArrayList<Person>();
        
        System.out.println("Number of persons is " + persons.size());
        persons.add(new Employee("Seth", 20, "80088888", "Math", 20000000000.0f));
        persons.add(new Employee("Bryan", 20, "80088888", "CS", 50000000000.0f));
        persons.add(new Student("Clarissa", 21, "80012345", "Mathematics"));
        persons.add(new Person("Bianca", 19));
        persons.add(new Student("NoahGreg", 40, "80012345", "Stuff"));
        persons.remove(1);
        System.out.println("Number of persons is " + persons.size());
        
        for (int i=0; i<persons.size(); i++)
        {
            System.out.println(persons.get(i));
            if (persons.get(i) instanceof Student)
            {
                System.out.println("Student " + i 
                        + " has id = " + ((Student)persons.get(i)).getId());
            }
            if (persons.get(i) instanceof Employee)
            {
                System.out.println("Employee " + i 
                        + " has salary = " + ((Employee)persons.get(i)).getSalary());
            }
        }
    }

}

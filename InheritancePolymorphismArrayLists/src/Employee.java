/**
 * All employees may be students.
 * 
 * @author dplante
 *
 */
public class Employee extends Student
{
    private float mySalary;
    
    public Employee(String name, int age, String id, String major, float salary)
    {
        super(name, age, id, major);
        mySalary = salary;
    }

    /**
     * This "toString()" method "overrides" the "toString()"
     * method inherited from the Object class.
     */
    public String toString()
    {
        return super.toString() + " salary = " + mySalary;
    }

    public float getSalary()
    {
        return mySalary;
    }

    public void setSalary(float salary)
    {
        mySalary = salary;
    }
}

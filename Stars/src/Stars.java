/**
 * Stars example we worked out together in class.
 * 
 * @author dplante
 *
 */
public class Stars
{

    public static void main(String[] args)
    {
        int numRows = 8;
        int numSpaces = numRows - 1;
        
        for (int i=0; i<numRows; i++)
        {
            /*
             * Draw just the leading spaces first
             */
            for (int j=0; j<numSpaces; j++)
            {
                System.out.print(" ");
            }
            
            /*
             * Draw "* " (asterisk followed space) 
             * correct number of times
             */
            for (int j=0; j<i+1; j++)
            {
                System.out.print("* ");
            }
            
            /*
             * Finish off line and decrement number
             * of spaces to draw next loop
             */
            System.out.println("");
            numSpaces--;
        }
    }

}

import java.util.Scanner;

/**
 * One example of using the Scanner class to input strings and print them out.
 * 
 * @author dplante
 *
 */
public class ScannerInput4
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your name, and enter DONE when finished: ");

        /*
         * Notice below that we developed (in class) a way to stop the Scanner
         * from looping to receive strings by stopping when "DONE" is entered.
         * Then, we read in a boolean (true or false) to print gender.
         */
        String str = "";
        while (!str.equals("DONE") && sc.hasNext())
        {
            str = sc.next();

            if (!str.equals("DONE"))
            {
                System.out.println(str);
            }
        }

        System.out.println("Are you a male?");
        boolean bool = sc.nextBoolean();

        if (bool)
        {
            System.out.println("You are a Male");
        } else
        {
            System.out.println("You are a female");
        }
        sc.close();
    }

}

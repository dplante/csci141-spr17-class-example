import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * A little help with using dates, formatting them for output, etc.
 * 
 * @author dplante
 * @version 1.0  -  10 November 2011
 */
public class Dates 
{
	public static void main(String[] args) 
	{
		Calendar calendar = new GregorianCalendar(2011, 11, 01);

		System.out.println("Year: " + calendar.get(Calendar.YEAR));
		System.out.println("Month: " + calendar.get(Calendar.MONTH));
		System.out.println("Days: " + calendar.get(Calendar.DAY_OF_MONTH));

		calendar.set(Calendar.MONTH, Calendar.MAY);
		calendar.set(Calendar.DAY_OF_MONTH, 17);

		// Format the date in any form you like!
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
		System.out.println(format.format(calendar.getTime()));
		format.applyPattern("MM-dd-yy");
		System.out.println(format.format(calendar.getTime()));
		format.applyPattern("MM-dd");
		System.out.println(format.format(calendar.getTime()));
	}
}

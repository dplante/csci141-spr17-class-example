import java.util.Random;

public class Coin
{
    /*
     * final  - cannot change (constant)
     * static - belongs to the class, not to each object
     */
    public final static int HEADS = 1;
    public final static int TAILS = 0;
    public final static int NOT_FLIPPED = -1;
    
    private int myTop;
    private Random myRandom;
    
    /**
     * Constructor
     */
    public Coin()
    {
        myTop = NOT_FLIPPED;
        myRandom = new Random();
    }
    
    /**
     * Flipped the coin to heads or tails
     * @return what was flipped
     */
    public int flip()
    {
        myTop = Math.abs(myRandom.nextInt() % 2);
        return myTop;
    }

    /*
     * Setters and getters (or mutators and accessors)
     */
    public int getTop()
    {
        return myTop;
    }

    public void setTop(int top)
    {
        myTop = top;
    }
}

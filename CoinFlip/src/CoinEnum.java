import java.util.Random;

public class CoinEnum
{
    /*
     * final  - cannot change (constant)
     * static - belongs to the class, not to each object
     */    
    public static enum Top {HEADS, TAILS, NOT_FLIPPED};
    
    private Top myTop;
    private Random myRandom;
    
    /**
     * Constructor
     */
    public CoinEnum()
    {
        myTop = Top.NOT_FLIPPED;
        myRandom = new Random();
    }
    
    /**
     * Flipped the coin to heads or tails
     * @return what was flipped
     */
    public Top flip()
    {
        int value;
        value = Math.abs(myRandom.nextInt() % 2);
        
        if (value == 0)
        {
            myTop = Top.HEADS;
        }
        else
        {
            myTop = Top.TAILS;
        }
        return myTop;
    }

    /*
     * Setters and getters (or mutators and accessors)
     */
    public Top getTop()
    {
        return myTop;
    }

    public void setTop(Top top)
    {
        myTop = top;
    }
}

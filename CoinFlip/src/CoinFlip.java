import java.util.Scanner;

public class CoinFlip
{

    public static void main(String[] args)
    {

        Scanner sc = new Scanner(System.in);
        
        System.out.println("Do you want to flip a coin? ");
        boolean flip = sc.nextBoolean();
        
        CoinEnum coin = new CoinEnum();
        
        /*
         * instead of "if (flip == true)" can just use "if (flip)"
         */
        if (flip)
        {
            coin.flip();
            if (coin.getTop() == CoinEnum.Top.NOT_FLIPPED)
            {
                System.out.println("Coin is not flipped");
            } 
            else if (coin.getTop() == CoinEnum.Top.HEADS)
            {
                System.out.println("Coin is HEADS");
            }
            else
            {
                System.out.println("Coin is TAILS");
            }
                
        }
    }

}

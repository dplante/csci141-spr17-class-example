import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Example code to demonstrate labels with images attached.
 * 
 * @author Daniel Plante
 *
 */
public class LabelsAndImages 
{
	public static void main(String[] args) 
	{
		JFrame frame = new JFrame("Labels and Images");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/*
		 * Read in images from the "images" folder
		 */
		ImageIcon icon1, icon2, icon3;
		icon1 = new ImageIcon("images/frog.gif");
		icon2 = new ImageIcon("images/gum.gif");
		icon3 = new ImageIcon("images/kangaroo.gif");
		
		/*
		 * Set up labels with images where all are centered in the view
		 */
		JLabel label1, label2, label3;
		label1 = new JLabel("Froggy Left", icon1, SwingConstants.CENTER);
		label2 = new JLabel("Gummy Right", icon2, SwingConstants.CENTER);
		label3 = new JLabel("Kangaroo Above", icon3, SwingConstants.CENTER);
		
		/*
		 * Position text in label relative to the image
		 */
		label2.setHorizontalTextPosition(SwingConstants.LEFT);
		label2.setVerticalTextPosition(SwingConstants.CENTER);
		label3.setVerticalTextPosition(SwingConstants.BOTTOM);
		label3.setHorizontalTextPosition(SwingConstants.CENTER);
		
		/*
		 * Create panel, give it a size and color, then "add" the components
		 * just created into the panel.
		 */
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		panel.setPreferredSize(new Dimension(400, 600));
		panel.add(label1);
		panel.add(label2);
		panel.add(label3);
		frame.getContentPane().add(panel);
		frame.pack();
	}

}

import javax.swing.JOptionPane;

/**
 * Basic JOptionPane input and displaying in a JOptionPane message box
 * 
 * @author dplante
 *
 */
public class JOptionPaneInput2 
{
	public static void main(String[] args) 
	{
		// Input using JOptionPane class
		String message = "Enter a comment: ";
		String str = JOptionPane.showInputDialog(message);
		System.out.println(str);
		JOptionPane.showMessageDialog(null, str, "Your comment", JOptionPane.INFORMATION_MESSAGE);
	}

}

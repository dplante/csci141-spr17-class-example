/**
 * Converter class to convert integers to binary
 * 
 * @author Daniel Plante
 * @author Michael Branton
 * 
 */
public class Converter
{

	/*
	 * Methods
	 */
    /**
     * Converts an integer to binary
     * 
     * @param input an integer in base 10
     * @return a base 2 binary string of 0s and 1s
     */
    public static String toBinary(int input)
    {
        String outString="";
        while(input != 0)
        {
            if(input % 2 == 1)
            {
                outString="1"+outString;
            }
            else
            {
                outString="0"+outString;
            }
            input=input/2;
        }
        return outString;
    }
}
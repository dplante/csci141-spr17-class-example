import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * Perrrtty GUI for our converter program.
 * 
 * @author Daniel Plante
 *
 */

@SuppressWarnings("serial")
public class Convert extends JFrame implements ActionListener
{
	/*
	 * Properties
	 */
    private Color myBackgroundColor;  
    private JPanel myIoPanel, myHeaderPanel, myPanel; 
    private JTextField myNumberIn,myNumberOut;
    private JLabel myInLabel,myOutLabel,myTitle;
    private Button myConvertButton;
    private Font myTitleFont; 
    private GridLayout myLayout; 

    /*
     * Methods
     */
    public static void main(String[] args)
    {
    	new Convert();
    }
    
    public Convert()
    {
       // instantiate instance variables needed
       myBackgroundColor     = new Color (195,195,195);
       myInLabel       = new JLabel("Integer");
       myNumberIn      = new JTextField(10);
       myConvertButton = new Button("--->");
       myNumberOut     = new JTextField(10);
       myOutLabel      = new JLabel("Binary");
       myTitle         = new JLabel("The Converter");
       myTitleFont     = new Font("TimesRoman",Font.ITALIC,24); 
       myLayout        = new GridLayout(1,5);
       
       // Panels
       myPanel         = new JPanel();
       myPanel.setLayout(new FlowLayout());
       myPanel.setPreferredSize(new Dimension(500, 150));

       myHeaderPanel   = new JPanel();
       myHeaderPanel.setLayout(new FlowLayout());
       myHeaderPanel.setPreferredSize(new Dimension(500, 50));
       
       myIoPanel       = new JPanel();      
       myIoPanel.setLayout(myLayout);
       myIoPanel.setPreferredSize(new Dimension(500, 50));
       
       // add title to header panel
       myTitle.setFont(myTitleFont);
       myTitle.setForeground(Color.RED);
       myHeaderPanel.add(myTitle);

       // add fields to panel and widgets
       myConvertButton.addActionListener(this);
       myIoPanel.add(myInLabel);
       myIoPanel.add(myNumberIn);
       myIoPanel.add(myConvertButton);
       myIoPanel.add(myNumberOut);
       myIoPanel.add(myOutLabel);
       
       myPanel.add(myHeaderPanel);
       myPanel.add(myIoPanel);
       this.getContentPane().add(myPanel);
       this.pack();

       this.setBackground(myBackgroundColor);
       this.setVisible(true);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    // look for the convert button to be pressed;
    //     in - the integer input
    public void actionPerformed(ActionEvent e)
    {
        int in;
        String inTxt, outTxt;
        
        if(e.getSource() == myConvertButton)
        {
            inTxt = myNumberIn.getText();
            in = Integer.parseInt(inTxt);
            outTxt = Converter.toBinary(in);
            myNumberOut.setText(outTxt);
        }
    }
}
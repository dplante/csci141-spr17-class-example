import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Example for reading and writing to files.
 * 
 * @author dplante
 *
 */
public class Files
{

    public static void main(String[] args)
    {
        new Files();
    }

    public Files()
    {
        /*
         * Open "input.txt" file for reading using Scanner class
         */
        Scanner scanner;
        try
        {
           // FileInputStream stream = new FileInputStream("input.txt");
            FileInputStream stream = new FileInputStream("numbers.txt");
            scanner = new Scanner(stream);
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        
        /*
         * Open "output.txt" file for printing to.
         */
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter("output.txt");
        } catch (FileNotFoundException e)
        {
            System.out.println(e.getMessage());
            System.exit(-1);
        } catch (SecurityException e)
        {
            System.out.println(e.getMessage());
            System.exit(-1);
        } 
        
        /*
         * Copy the text in the "input.txt" file to the "output.txt" file
         */
        int total = 0;
        while (scanner.hasNextLine())
        {
            int value = scanner.nextInt();
            total += value;
//            String line = scanner.nextLine();
//            pw.println(line);
        }
        pw.println("Total = " + total);

        pw.close();
    }

}

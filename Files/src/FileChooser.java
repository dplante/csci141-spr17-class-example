import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Example for using JFileChooser to choose a file and to read from
 * and write to a file.
 * 
 * @author dplante
 *
 */
public class FileChooser
{

    public static void main(String[] args)
    {
        new FileChooser();
    }

    public FileChooser()
    {
        JFrame frame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose a file to open");
        fileChooser.setSelectedFile(null);
        
        /*
         * Set the directory to be your home directory using "user.home" or to the 
         * present directory using "." as shown below.
         */
        //fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        fileChooser.setCurrentDirectory(new File("."));
        
        int returnVal = fileChooser.showOpenDialog(null);
        
        // User canceled or clicked the dialog’s close box.
        if (returnVal != JFileChooser.APPROVE_OPTION)
        {
            return; 
        }
        
        File selectedFile = fileChooser.getSelectedFile();
        System.out.println("Selected file: " + selectedFile.getAbsolutePath());
        
        /*
         * Pass the selected file to the input stream, which you can then pass
         * to the Scanner to now read data from the file just the same as you
         * would do with Scanner reading from the console.
         */
        Scanner scanner;
        try
        {
            FileInputStream stream = new FileInputStream(selectedFile);
            //FileInputStream stream = new FileInputStream("junk");
            scanner = new Scanner(stream);
        } catch (Exception e)
        {
            JOptionPane.showMessageDialog(frame, "Sorry, but an error occurred while trying to open the file:\n" + e);
            return;
        }
        
        /*
         * The PrintWriter takes in an argument for the output file, though 
         * you must catch the possible exceptions it throws.
         */
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter("chooser_output.txt");
        } catch (FileNotFoundException e)
        {
            System.out.println(e.getMessage());
            System.exit(-1);
        } catch (SecurityException e)
        {
            System.out.println(e.getMessage());
            System.exit(-1);
        } 
        
        /*
         * Loop through the input file and print those lines to the output file.
         */
        while (scanner.hasNextLine())
        {
            String line = scanner.nextLine();
            pw.println(line);
        }

        /*
         * Don't forget to close your scanner and PrintWriter.
         */
        scanner.close();
        pw.close();
    }

}
